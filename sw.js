importScripts('workbox-sw.prod.v2.1.2.js');

/**
 * DO NOT EDIT THE FILE MANIFEST ENTRY
 *
 * The method precache() does the following:
 * 1. Cache URLs in the manifest to a local cache.
 * 2. When a network request is made for any of these URLs the response
 *    will ALWAYS comes from the cache, NEVER the network.
 * 3. When the service worker changes ONLY assets with a revision change are
 *    updated, old cache entries are left as is.
 *
 * By changing the file manifest manually, your users may end up not receiving
 * new versions of files because the revision hasn't changed.
 *
 * Please use workbox-build or some other tool / approach to generate the file
 * manifest which accounts for changes to local files and update the revision
 * accordingly.
 */
const fileManifest = [
  {
    "url": "css/base.css",
    "revision": "1f3c281e13b05dedcf423bae670c161d"
  },
  {
    "url": "css/bootstrap.material.css",
    "revision": "840df82985d3b2a4b3ee41ce91358737"
  },
  {
    "url": "css/index.css",
    "revision": "72617bbb18b74af74e6de58e88eaf837"
  },
  {
    "url": "favicon.ico",
    "revision": "433d1246d222f822fa701d158cb8e157"
  },
  {
    "url": "images/icons/icon-128x128.png",
    "revision": "af71413a0cb667b0a5718c825c06fa31"
  },
  {
    "url": "images/icons/icon-144x144.png",
    "revision": "05d05771aa12f558124460b261c5a116"
  },
  {
    "url": "images/icons/icon-152x152.png",
    "revision": "89ec17814ad3e05c3cf8c764f6c6d4b2"
  },
  {
    "url": "images/icons/icon-192x192.png",
    "revision": "6b2a7dc7c56d8bdbc8ab0c670e964409"
  },
  {
    "url": "images/icons/icon-512x512.png",
    "revision": "b6a547e251005a833c1797765460b5e6"
  },
  {
    "url": "images/icons/icon-72x72.png",
    "revision": "983480b89b7719f4279f8e4d5bb9af61"
  },
  {
    "url": "images/icons/icon-96x96.png",
    "revision": "de7525d1a41abcfd0eccdd9f95ae8709"
  },
  {
    "url": "index.html",
    "revision": "707f3ef6feec747da9d0f90fa8dad003"
  },
  {
    "url": "js/app.js",
    "revision": "e8bc7e3b46e3d868e0549f618d5043c8"
  },
  {
    "url": "js/base.js",
    "revision": "3bcbf1e6206abb2effa0677bfe7cf8f3"
  },
  {
    "url": "js/controller.js",
    "revision": "be0578f2970270de84e99404aee496b6"
  },
  {
    "url": "js/helpers.js",
    "revision": "1c6bfda95abf09b59450794e9a4e4bae"
  },
  {
    "url": "js/model.js",
    "revision": "f8f75c6cea15c518318f28606d577278"
  },
  {
    "url": "js/store.js",
    "revision": "dd80435d78104b5b6a264e7a14148cef"
  },
  {
    "url": "js/template.js",
    "revision": "59a685f6e4311a6feda6c40062acac70"
  },
  {
    "url": "js/view.js",
    "revision": "96ea5eebc666cbab34d5629eb1bf9380"
  }
];

const workboxSW = new self.WorkboxSW();
workboxSW.precache(fileManifest);
