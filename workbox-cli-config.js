module.exports = {
  "globDirectory": "./",
  "globPatterns": [
    "**/*.{css,ico,png,html,js}"
  ],
  "swDest": "./sw.js",
  "globIgnores": [
    "workbox-cli-config.js"
  ]
};
